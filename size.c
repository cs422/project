#include <stdio.h>
#include <math.h>
#include <stdlib.h>
int budget = 32*1024*8/4;
int bhtEntry = 64;
int counter = 2;

int main(){
	printf("%d\n",sizeof(unsigned) );
	int history;
	for(history = 4; history < 20;history++){
		double size = (budget - pow(2,history)*counter)/(bhtEntry + history);
		int bits = log2(size);
		long long total = pow(2,bits)*(bhtEntry+history) + (pow(2,history))*counter;
		printf("%d  %f %d %lld %d\n",history, size, bits,total,budget);
	}
}