#include "pin.H"
#include <iostream>
#include <stdlib.h>
#include <fstream>
#include <iomanip>
#include <limits.h>
#include <math.h>
/* ================================================================== */
// Global variables 
/* ================================================================== */
#define LL long long unsigned
#define BUDGET 32*1024*8
std::ostream *out = &cerr;
UINT64 fast_forward = 0;
UINT64 icount = 0;
char tab = '\t';
UINT64 totalLoadCalls = 0;

//last value
struct lastValBHTentry{
	unsigned history;
	LL predVal;
};
#define lastValueBits 8*8
#define lastValueHistoryBits 15
#define lastValueCounterBits 2
UINT64 lastValueCorrectPredictions = 0;
UINT64 lastValueIncorrectPredictions = 0;
unsigned lastValueThreshold =  1 << (lastValueCounterBits-1);
const unsigned lastValuePHTsize = 1 << lastValueHistoryBits;
unsigned lastValuePHT[lastValuePHTsize] = {0};
//const unsigned lastValueBHTsize = 1 << 11; //(unsigned)(log2((unsigned)((BUDGET - lastValuePHTsize*lastValueCounterBits)/(lastValueHistoryBits + lastValueBits))));
const unsigned lastValueBHTsize = 1 << 11;
lastValBHTentry lastValueBHT[lastValueBHTsize];

const unsigned lastValueSimpleSize = (unsigned)(BUDGET/lastValueBits);
LL lastValueSimple[lastValueSimpleSize];
UINT64 lastValueSimpleCorrectPredictions = 0;

//stride 2-delta
struct strideBHTentry{
	LL baseValue;
	int stride1; //8 bits
	int stride2; // 8 bits 
	unsigned history;
};
struct strideSimpleEntry{
	LL baseValue;
	int stride1; //8 bits
	int stride2; // 8 bits 
};
#define strideCounterBits 2
#define strideHistoryBits 15
#define strideBits 8
unsigned strideThreshold = 1 << (strideCounterBits - 1);
const unsigned stridePHTsize = 1 << strideHistoryBits;
unsigned stridePHT[stridePHTsize] = {0};
const unsigned strideBHTsize = 1 << 11;
strideBHTentry strideBHT[strideBHTsize];
UINT64 strideCorrectPredictions = 0;
UINT64 strideIncorrectPredictions = 0;
UINT64 strideSimpleCorrectPredictions = 0 ;
UINT64 strideSimpleIncorrectPredictions = 0;
const unsigned strideSimpleSize = 4096;
strideSimpleEntry strideSimple[strideSimpleSize];


// last four value BIMODAL
#define fourValueIndexBits 10
#define fourValueCounterBits 2
struct fourValueEntry{
	LL value;
	unsigned counter;
};
struct fourValuePHTentry{
	fourValueEntry slot1;
	fourValueEntry slot2;
	fourValueEntry slot3;
	fourValueEntry slot4;
};
const unsigned fourValuePHTsize = 1 << fourValueIndexBits;
fourValuePHTentry fourValuePHT[fourValuePHTsize];
unsigned fourValueCorrectPredictions,fourValueIncorrectPredictions,fourValueSimpleCorrectPredictions,fourValueSimpleIncorrectPredictions;

// last four value sag

#define fourValueSAGhistoryBits 13
#define fourValueSAGindexBits 9
const unsigned fourValueSAG_BHTsize = 1 << fourValueSAGindexBits;
const unsigned fourValueSAG_PHTsize = 1 << fourValueSAGhistoryBits;

struct fourValueBHTentry{
	int history;
	LL prediction;
};

struct fourValueSAG{
	fourValueBHTentry BHT[fourValueSAG_BHTsize];
	unsigned PHT[fourValueSAG_PHTsize];
};

fourValueSAG fourValueSAGpredictor[4];

unsigned fourValueSAGcorrectPredictions,fourValueSAGincorrectPredictions;

//hybrid of last four value and stride 2-delta
#define hybridStrideHistoryBits 10
#define hybridCounterBits 2
#define hybridIndexBits 9
const unsigned hybridSize =  1 << hybridIndexBits;
strideBHTentry hybridStrideBHT[hybridSize];
const unsigned hybridStridePHTsize = 1 << hybridStrideHistoryBits;
unsigned hybridStridePHT[hybridStridePHTsize];
fourValuePHTentry hybridFourValuePHT[hybridSize];
unsigned hybridCorrectFourValuePredictions,hybridCorrectStridePredictions,hybridIncorrectFourValuePredictions,hybridIncorrectStridePredictions;

KNOB<string> KnobOutputFile(KNOB_MODE_WRITEONCE,  "pintool",
    "o", "", "specify file name for output");

KNOB<UINT32> KnobAmount(KNOB_MODE_WRITEONCE,  "pintool",
    "f", "0", "specify fast-forward amount in billions");

INT32 Usage()
{
    cerr << "This tool prints out the number of dynamically executed " << endl <<
            "instructions, basic blocks and threads in the application." << endl << endl;

    cerr << KNOB_BASE::StringKnobSummary() << endl;

    return -1;
}

VOID InsCount()  {
    icount++;
}

LL
get(ADDRINT pc, lastValBHTentry* BHT,unsigned BHTsize, unsigned *PHT,unsigned PHTsize, unsigned threshold , bool* toUpdate){
	unsigned count = PHT[(BHT[(pc & (BHTsize - 1))].history) & (PHTsize - 1)];
	if(count < threshold)
		*toUpdate = 0;
	else 
		*toUpdate = 1;	
	return BHT[(pc & (BHTsize - 1))].predVal; 
}

void
update(ADDRINT pc, lastValBHTentry* BHT, unsigned BHTsize, unsigned *PHT,unsigned PHTsize, unsigned threshold, LL readValue, bool predicted){
	unsigned Bindex = pc & (BHTsize - 1);
	unsigned Pindex = BHT[Bindex].history & (PHTsize -1 );
	unsigned count = PHT[Pindex];
	if(predicted  && count < threshold - 1)
		PHT[Pindex]++;
	else if(!predicted && count > 0)
		PHT[Pindex]--;
	BHT[Bindex].predVal = readValue;
	BHT[Bindex].history = ( Pindex << 1) | predicted;
}

ADDRINT Terminate(void)
{
        return (icount >= (fast_forward + 1000000000));
}

// Analysis routine to check fast-forward condition
ADDRINT FastForward(void) {
    return (icount >= fast_forward && icount < (fast_forward + 1000000000));
}

void dec(fourValueEntry* temp){
	if(temp->counter > 0)
		temp->counter--;
}

void inc(fourValueEntry* temp,unsigned threshold){
	if(temp->counter < threshold)
		temp->counter++;
}

LL fourValueSAGutility(fourValueSAG* predictor,ADDRINT pc,LL readValue, unsigned* conf){
	unsigned Bindex,Pindex,count;
	LL prediction;
	Bindex = pc & (fourValueSAG_BHTsize - 1);
	Pindex = predictor->BHT[Bindex].history & (fourValueSAG_PHTsize - 1 );
	count = predictor->PHT[Pindex];
	prediction = predictor->BHT[Bindex].prediction;
	*conf = count;

	unsigned threshold = (1 << fourValueCounterBits) - 1;
	if(prediction == readValue && count < threshold)
		predictor->PHT[Pindex]++;
	else if(prediction != readValue && count > 0)
		predictor->PHT[Pindex]--;
	predictor->BHT[Bindex].history = (Pindex << 1) | (prediction == readValue);
	return prediction;
}

VOID analysisRoutine(ADDRINT pc,UINT32 opIndex, ADDRINT *addr, UINT32 numBytes){
	// for all
	unsigned Bindex,Pindex,count,index;
	bool toPredict, correctPrediction;
	pc = (pc << 1) | opIndex ;
	LL readValue;
	LL prediction;
	memset((void*)&readValue,0,8);
	PIN_SafeCopy((VOID *)(&readValue),addr,numBytes);


	// =========================================================================================
	//-----------last Value-------------------------
	//simple
	index = pc & (lastValueSimpleSize -1);
	prediction = lastValueSimple[index];
	if(prediction == readValue)
		lastValueSimpleCorrectPredictions++;
	lastValueSimple[index] = readValue;

	//sag
	prediction = get(pc,lastValueBHT,lastValueBHTsize,lastValuePHT,lastValuePHTsize,lastValueThreshold,&toPredict);
	if(toPredict && prediction == readValue)
		lastValueCorrectPredictions++;
	else if(toPredict && prediction != readValue)
		lastValueIncorrectPredictions++;
	update(pc,lastValueBHT,lastValueBHTsize,lastValuePHT,lastValuePHTsize,lastValueThreshold << 1,readValue,prediction == readValue);

	// =========================================================================================
	// ------------------------------stride 2-delta------------------------------
	//sag
	Bindex = pc & (strideBHTsize - 1);
	Pindex = strideBHT[Bindex].history & (stridePHTsize - 1 );
	count = stridePHT[Pindex];
	index = pc & (strideSimpleSize - 1);
	if(strideBHT[Bindex].stride1 == strideBHT[Bindex].stride2){
		prediction = strideBHT[Bindex].baseValue + strideBHT[Bindex].stride1;
		if(count < strideThreshold)
			toPredict = 0;
		else 
			toPredict = 1;	
		correctPrediction = (prediction == readValue);
		if(toPredict && correctPrediction)
			strideCorrectPredictions++;
		if(toPredict && !correctPrediction)
			strideIncorrectPredictions++;
		strideBHT[Bindex].stride2 = strideBHT[Bindex].stride1;
		strideBHT[Bindex].stride1 = (readValue - strideBHT[Bindex].baseValue ) & ((1 << strideBits )- 1);
		strideBHT[Bindex].baseValue = readValue;
		if(correctPrediction  && count < 2*strideThreshold - 1)
			stridePHT[Pindex]++;
		else if(!correctPrediction && count > 0)
			stridePHT[Pindex]--;
		strideBHT[Bindex].history = ( Pindex << 1) | correctPrediction;
	}
	//simple
	if(strideSimple[index].stride1 == strideSimple[index].stride2){
		if(strideSimple[index].baseValue + strideBHT[Bindex].stride1 == readValue)
			strideSimpleCorrectPredictions++;
		else
			strideSimpleIncorrectPredictions++;
	}
	strideSimple[index].stride2 = strideSimple[index].stride1;
	strideSimple[index].stride1 = (readValue - strideSimple[index].baseValue ) & ((1 << strideBits )- 1);
	strideSimple[index].baseValue = readValue;
	// =========================================================================================
	//------------- last four value bimodal ---------------
	unsigned threshold = 1 << (fourValueCounterBits  - 1);
	fourValueEntry* tempPHTentry;
	fourValuePHTentry* currPHTentry;
	Bindex = pc & ( fourValuePHTsize -1 );
	currPHTentry = fourValuePHT + Bindex;
	tempPHTentry = &(currPHTentry->slot1);
	if(tempPHTentry->counter < currPHTentry->slot2.counter)
		tempPHTentry = &(currPHTentry->slot2);
	if(tempPHTentry->counter < currPHTentry->slot3.counter)
		tempPHTentry = &(currPHTentry->slot3);
	if(tempPHTentry->counter < currPHTentry->slot4.counter)
		tempPHTentry = &(currPHTentry->slot4);
	prediction = tempPHTentry->value;
	if(tempPHTentry->counter >= threshold){
		if(prediction == readValue)
			fourValueCorrectPredictions++;
		else
			fourValueIncorrectPredictions++;
	}
	if(prediction == readValue)
		fourValueSimpleCorrectPredictions++;
	else
		fourValueSimpleIncorrectPredictions++;

	if(currPHTentry->slot1.value == readValue)
		tempPHTentry = &(currPHTentry->slot1);
	else if(currPHTentry->slot2.value == readValue)
		tempPHTentry = &(currPHTentry->slot2);
	else if(currPHTentry->slot3.value == readValue)
		tempPHTentry = &(currPHTentry->slot3);
	else if(currPHTentry->slot4.value == readValue)
		tempPHTentry = &(currPHTentry->slot4);
	else
		tempPHTentry = NULL;

	if(tempPHTentry == &(currPHTentry->slot1))
		inc(tempPHTentry,threshold*2-1);
	else
		dec(&(currPHTentry->slot1));
	if(tempPHTentry == &(currPHTentry->slot2))
		inc(tempPHTentry,threshold*2-1);
	else
		dec(&(currPHTentry->slot2));
	if(tempPHTentry == &(currPHTentry->slot3))
		inc(tempPHTentry,threshold*2-1);
	else
		dec(&(currPHTentry->slot3));
	if(tempPHTentry == &(currPHTentry->slot4))
		inc(tempPHTentry,threshold*2-1);
	else
		dec(&(currPHTentry->slot4));
	currPHTentry->slot4.value = currPHTentry->slot3.value;
	currPHTentry->slot3.value = currPHTentry->slot2.value;
	currPHTentry->slot2.value = currPHTentry->slot1.value;
	currPHTentry->slot1.value = readValue;

	//--------last four value SAG---------------
	LL predictionFourValue[4];
	LL finalPrediction;
	int i;
	unsigned conf[4];
	for (int i= 0 ;i < 4;i++)
		predictionFourValue[i] = fourValueSAGutility(fourValueSAGpredictor+i,pc,readValue,conf+i);
	unsigned maxConf = conf[0];
	finalPrediction = predictionFourValue[0];
	for(i=1;i<4;i++){
		if(conf[i]>maxConf){
			maxConf = conf[i];
			finalPrediction = predictionFourValue[i];
		}
	}
	threshold = 1 << (fourValueCounterBits-1);
	if(maxConf >= threshold){
		if(finalPrediction == readValue)
			fourValueSAGcorrectPredictions++;
		else
			fourValueSAGincorrectPredictions++;
	}
	Bindex = pc & (fourValueSAG_BHTsize - 1);
	for(i=3;i>0;i--){
		fourValueSAGpredictor[i].BHT[Bindex].prediction = fourValueSAGpredictor[i-1].BHT[Bindex].prediction;
	}
	fourValueSAGpredictor[0].BHT[Bindex].prediction = readValue;

	//==========================================================================================
	// hybrid of stride 2-delta and four value
	bool strideHasPrediction,fourValueHasPrediction;
	LL stridePrediction = 0,fourValuePredicton = 0;
	int strideConf,fourValueConf;
	// stride part
	Bindex = pc & (hybridSize - 1);
	Pindex = hybridStrideBHT[Bindex].history & (hybridStridePHTsize - 1 );
	count = hybridStridePHT[Pindex];
	strideConf = count;
	if(hybridStrideBHT[Bindex].stride1 == hybridStrideBHT[Bindex].stride2){
		if(count < strideThreshold)
			strideHasPrediction = false;
		else
			strideHasPrediction = true;
		stridePrediction = hybridStrideBHT[Bindex].baseValue + hybridStrideBHT[Bindex].stride1;	
		correctPrediction = (stridePrediction == readValue);
		hybridStrideBHT[Bindex].stride2 = hybridStrideBHT[Bindex].stride1;
		hybridStrideBHT[Bindex].stride1 = (readValue - hybridStrideBHT[Bindex].baseValue ) & ((1 << strideBits )- 1);
		hybridStrideBHT[Bindex].baseValue = readValue;
		if(correctPrediction  && count < 2*strideThreshold - 1)
			hybridStridePHT[Pindex]++;
		else if(!correctPrediction && count > 0)
			hybridStridePHT[Pindex]--;
		hybridStrideBHT[Bindex].history = ( Pindex << 1) | correctPrediction;
	}
	else
		strideHasPrediction = false;

	// last four value part
	threshold = 1 << (hybridCounterBits  - 1);
	currPHTentry = hybridFourValuePHT + Bindex;
	tempPHTentry = &(currPHTentry->slot1);
	if(tempPHTentry->counter < currPHTentry->slot2.counter)
		tempPHTentry = &(currPHTentry->slot2);
	if(tempPHTentry->counter < currPHTentry->slot3.counter)
		tempPHTentry = &(currPHTentry->slot3);
	if(tempPHTentry->counter < currPHTentry->slot4.counter)
		tempPHTentry = &(currPHTentry->slot4);

	if(tempPHTentry->counter >= threshold)
		fourValueHasPrediction = true;
	else
		fourValueHasPrediction = false;

	fourValuePredicton = tempPHTentry->value;
	fourValueConf = tempPHTentry->counter;
	
	if(currPHTentry->slot1.value == readValue)
		tempPHTentry = &(currPHTentry->slot1);
	else if(currPHTentry->slot2.value == readValue)
		tempPHTentry = &(currPHTentry->slot2);
	else if(currPHTentry->slot3.value == readValue)
		tempPHTentry = &(currPHTentry->slot3);
	else if(currPHTentry->slot4.value == readValue)
		tempPHTentry = &(currPHTentry->slot4);
	else
		tempPHTentry = NULL;

	if(tempPHTentry == &(currPHTentry->slot1))
		inc(tempPHTentry,threshold*2-1);
	else
		dec(&(currPHTentry->slot1));
	if(tempPHTentry == &(currPHTentry->slot2))
		inc(tempPHTentry,threshold*2-1);
	else
		dec(&(currPHTentry->slot2));
	if(tempPHTentry == &(currPHTentry->slot3))
		inc(tempPHTentry,threshold*2-1);
	else
		dec(&(currPHTentry->slot3));
	if(tempPHTentry == &(currPHTentry->slot4))
		inc(tempPHTentry,threshold*2-1);
	else
		dec(&(currPHTentry->slot4));
	currPHTentry->slot4.value = currPHTentry->slot3.value;
	currPHTentry->slot3.value = currPHTentry->slot2.value;
	currPHTentry->slot2.value = currPHTentry->slot1.value;
	currPHTentry->slot1.value = readValue;

	// checking the predictions
	if(fourValueConf > strideConf && fourValueHasPrediction){
		if(fourValuePredicton == readValue){
			hybridCorrectFourValuePredictions++;
			hybridCorrectStridePredictions++;
		}
		else{
			hybridIncorrectFourValuePredictions++;
			hybridIncorrectStridePredictions++;
		}
	}
	else if(fourValueConf < strideConf && strideHasPrediction){
		if(stridePrediction == readValue){
			hybridCorrectFourValuePredictions++;
			hybridCorrectStridePredictions++;
		}
		else{
			hybridIncorrectFourValuePredictions++;
			hybridIncorrectStridePredictions++;
		}
	}
	//conflicting predictions
	else if(fourValueConf == strideConf && fourValueHasPrediction){
		//stride gets priority
		if(stridePrediction == readValue)
			hybridCorrectStridePredictions++;
		else
			hybridIncorrectStridePredictions++;
		// four value gets priority
		if(fourValuePredicton == readValue)
			hybridCorrectFourValuePredictions++;
		else
			hybridIncorrectFourValuePredictions++;
	}
//=====================================================================================================================
	// for all
	totalLoadCalls++;
}


VOID printOne(UINT64 correctP,UINT64 incorrectP){
	char correct[] = "Correct:";
	char incorrect[] = "Incorrect:";
	char accuracy[] = "Accuracy:";
	char overall[] = "Overall_Accuracy:";
	// char correct[] = "";
	// char incorrect[] = "";
	// char accuracy[] = "";
	// char overall[] = "";
	*out  << correct << correctP << tab << incorrect << incorrectP << tab << accuracy << std::fixed << std::setprecision(4) << (double)correctP/(double)(correctP + incorrectP)  << tab << overall << (double)(correctP)/(double)(totalLoadCalls) << endl; 

}

VOID printBoth(UINT64 correctP,UINT64 incorrectP,UINT64 simpleCP,UINT64 simpleIP){
	char conf[] = "Using Confidence Estimators -- ";
	char noConf[] = "Without using Confidence Estimators -- ";
	char correct[] = "Correct:";
	char incorrect[] = "Incorrect:";
	char accuracy[] = "Accuracy:";
	char overall[] = "Overall_Accuracy:";

	// char conf[] = "";
	// char noConf[] = "";
	// char correct[] = "";
	// char incorrect[] = "";
	// char accuracy[] = "";
	// char overall[] = "";
    *out << conf << tab << correct << correctP << tab << incorrect << incorrectP << tab << accuracy << std::fixed << std::setprecision(4) << (double)(correctP)/(double)(incorrectP + correctP) << tab << overall << (double)(correctP)/(double)(totalLoadCalls) << endl;
    *out << noConf << tab << correct << simpleCP << tab << incorrect << simpleIP << tab << accuracy << std::fixed << std::setprecision(4) << (double)(simpleCP)/(double)(simpleCP + simpleIP) <<  tab << overall << (double)(simpleCP)/(double)(totalLoadCalls) << endl;

}
// Analysis routine to exit the application
VOID MyExitRoutine() {
	char sep[] = " ========= ";
	*out << "Total load ins:" << totalLoadCalls << endl << endl;
	*out << sep << "last Value Predictions" << sep << endl;
	string fileName = KnobOutputFile.Value();
	if (!fileName.empty()) { out = new std::ofstream(fileName.c_str());}
    *out << sep << "Last Value" << sep << endl;
	printBoth(lastValueCorrectPredictions,lastValueIncorrectPredictions,lastValueSimpleCorrectPredictions,totalLoadCalls - lastValueSimpleCorrectPredictions);
    *out << sep << "Stride 2-Delta" << sep << endl;
    printBoth(strideCorrectPredictions,strideIncorrectPredictions,strideSimpleCorrectPredictions,strideSimpleIncorrectPredictions);
    *out << sep << "Last Four Value using BIMODAL" << sep << endl;
    printBoth(fourValueCorrectPredictions,fourValueIncorrectPredictions,fourValueSimpleCorrectPredictions,fourValueSimpleIncorrectPredictions);
    *out << sep << "Last Four Value using SAG Confidence Estimator" << sep << endl;
    printOne(fourValueSAGcorrectPredictions,fourValueSAGincorrectPredictions);
    *out << sep << "Hybrid of Stride 2-delta and last four value with priority to stride 2-delta" << sep << endl;
    printOne(hybridCorrectStridePredictions,hybridIncorrectStridePredictions);
    *out << sep << "Hybrid of Stride 2-delta and last four value with priority to last four value" << sep << endl;
    printOne(hybridCorrectFourValuePredictions,hybridIncorrectFourValuePredictions);
    exit(0);
}

VOID analyze(INS ins, VOID *v)
{
	INS_InsertIfCall(ins, IPOINT_BEFORE, (AFUNPTR)Terminate, IARG_END);
    INS_InsertThenCall(ins, IPOINT_BEFORE, (AFUNPTR)MyExitRoutine,  IARG_END);
	UINT32 n = INS_MemoryOperandCount(ins);
	for(UINT32 i = 0;i<n;i++){
	    if(INS_MemoryOperandIsRead(ins,i) ){
	        INS_InsertIfCall(ins, IPOINT_BEFORE, (AFUNPTR)FastForward, IARG_END);
	        INS_InsertThenPredicatedCall(ins,IPOINT_BEFORE,(AFUNPTR)analysisRoutine,IARG_INST_PTR,IARG_UINT32, i,IARG_MEMORYREAD_EA,IARG_UINT32,INS_MemoryOperandSize(ins,i),IARG_END);
		    if(i < n-1 && INS_HasMemoryRead2(ins) ){
		        i++;
		        INS_InsertIfCall(ins, IPOINT_BEFORE, (AFUNPTR)FastForward, IARG_END);
		        INS_InsertThenPredicatedCall(ins,IPOINT_BEFORE,(AFUNPTR)analysisRoutine,IARG_INST_PTR,IARG_UINT32, i,IARG_MEMORYREAD2_EA,IARG_UINT32,INS_MemoryOperandSize(ins,i),IARG_END);
		    }
		}
	}
	//common to all
	INS_InsertCall(ins, IPOINT_BEFORE, (AFUNPTR)InsCount, IARG_END);
}


VOID Fini(INT32 code, VOID *v)
{
	*out <<  "===============================================" << endl;
	*out <<  "MyPinTool analysis results: " << endl;
	*out <<  "Number of instructions: " << icount  << endl;
	*out << "fast_forward count is: " << fast_forward << endl;
	*out <<  "===============================================" << endl;
	MyExitRoutine();
}


int main(int argc, char *argv[])
{
	// Initialize PIN library. Print help message if -h(elp) is specified
	// in the command line or the command line is invalid 
	if( PIN_Init(argc,argv) )
	{
		return Usage();
	}
	//common
	icount = 0;
	fast_forward = (UINT64)(KnobAmount.Value())*1000000000; 
	unsigned i;

	// last value
	lastValueCorrectPredictions = lastValueIncorrectPredictions = 0;
	lastValueSimpleCorrectPredictions = 0;
	for(i=0;i<lastValuePHTsize;i++)
		lastValuePHT[i] = 0;
	for(i=0;i<lastValueBHTsize;i++){
		lastValueBHT[i].history = 0;
		lastValueBHT[i].predVal = 0;
	}
	for(i=0;i<lastValueSimpleSize;i++)
		lastValueSimple[i] = 0;
	//stride
	strideCorrectPredictions = strideIncorrectPredictions = strideSimpleIncorrectPredictions = strideSimpleCorrectPredictions = 0;
	memset(strideSimple,strideSimpleSize*sizeof(strideSimpleEntry),0);
	for(i=0;i<stridePHTsize;i++)
		stridePHT[i]= 0;
	for(i=0;i<strideBHTsize;i++){
		strideBHT[i].stride1 = strideBHT[i].stride2  =  strideBHT[i].baseValue =  strideBHT[i].history = 0;
	}
	for(i=0;i<strideSimpleSize;i++)
		strideSimple[i].stride1 = strideSimple[i].stride2 = strideSimple[i].baseValue = 0;
	//last four value bimodal
	fourValueCorrectPredictions = fourValueIncorrectPredictions = fourValueSimpleCorrectPredictions = fourValueSimpleIncorrectPredictions = 0;
	memset(fourValuePHT,fourValuePHTsize*sizeof(fourValuePHTentry),0);
	// lsat four value sag
	memset(fourValueSAGpredictor,4*(sizeof(fourValueSAG)),0);
	fourValueSAGcorrectPredictions = fourValueSAGincorrectPredictions = 0;
	//hybrid of last four value and stride 2-delta
	memset(hybridStridePHT,hybridStridePHTsize*4,0);
	memset(hybridStrideBHT,sizeof(strideBHTentry)*hybridSize,0);
	memset(hybridFourValuePHT,sizeof(fourValuePHTentry)*hybridSize,0);
	hybridCorrectFourValuePredictions = hybridCorrectStridePredictions = hybridIncorrectFourValuePredictions = hybridIncorrectStridePredictions = 0;
	// Register function to be called to instrument traces
	INS_AddInstrumentFunction(analyze, 0);

	// Register function to be called when the application exits
	PIN_AddFiniFunction(Fini, 0);

	cerr <<  "===============================================" << endl;
	cerr <<  "This application is instrumented by MyPinTool" << endl;
	if (!KnobOutputFile.Value().empty()) 
	{
		cerr << "See file " << KnobOutputFile.Value() << " for analysis results" << endl;
	}
	cerr << "value of fast-forward count is: " << fast_forward << endl;
	cerr <<  "===============================================" << endl;

	// Start the program, never returns
//	*out << "total\t" << "current\t" << "IsRead\t" << "IsWritten\t" << "Size" << endl;
	PIN_StartProgram();

	return 0;
}

/* ===================================================================== */
/* eof */
/* ===================================================================== */

